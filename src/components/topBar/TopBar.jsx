import './topBar.scss'
import { Person, Mail } from '@mui/icons-material'

function TopBar({ menuOpen, setMenuOpen }) {
  return (
    <div className={'topBar ' + (menuOpen && 'active')}>
      <div className='wrapper'>
        <div className='left'>
          <a href='#intro' className='logo'>
            genius.
          </a>
          <div className='itemcontainer'>
            <Person className='icon' />
            <span> 094232633</span>
          </div>
          <div className='itemcontainer'>
            <Mail className='icon' />
            <span> safak@genius.com</span>
          </div>
        </div>

        <div className='right'>
          <div className='humburger' onClick={() => setMenuOpen(!menuOpen)}>
            <span className='line1'></span>
            <span className='line2'></span>
            <span className='line3'></span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default TopBar
